from giorno_4.esercizio_6 import *


def step1():
    print('########### Senza controllo di autenticazione')
    user = User()
    view_1(user, 1, 2)
    view_2(user, 5, 6)


def step2():
    user = User()
    print('########### Con controllo di autenticazione in ogni view')
    print('# Utente non Loggato')
    view_1_no_deco(user, 1, 2)
    view_2_no_deco(user, 5, 6)
    print('# Utente Loggato')
    user = view_login(user)
    view_1_no_deco(user, 1, 2)
    view_2_no_deco(user, 5, 6)


def step3():
    print('########## Controllo autenticazione tramite decoratore')
    print('# Utente non loggato')
    user = User()
    view_1_dec(user, 1, 2)
    view_2_dec(user, 5, 6)
    print('# Utente loggato')
    user = view_login(user)
    view_1_dec(user, 1, 2)
    view_2_dec(user, 5, 6)

    print('Nome funzione decorata: {}  '.format(view_1_dec.__name__))


def step4():
    print('########## Controllo autenticazione tramite decoratore modificato')
    print('# Utente non loggato')
    user = User()
    view_1_dec_wrap(user, 1, 2)
    view_2_dec_wrap(user, 5, 6)
    print('# Utente loggato')
    user = view_login(user)
    view_1_dec_wrap(user, 1, 2)
    view_2_dec_wrap(user, 5, 6)

    print('Nome funzione decorata: {}'.format(view_1_dec_wrap.__name__))

if __name__ == '__main__':
    step4()

