from functools import wraps


def logged(func):
    @wraps(func)
    def inner_func(*args, **kwargs):
        # codice che logga i parametri
        print('Parametri args: ', ', '.join([str(a) for a in args]))
        if kwargs:
            print('Parametri kwargs: ', ', '.join(['{}={}'.format(k, v)
                                                   for k, v in kwargs.items()]))
        res = func(*args, **kwargs)
        # codice che logga il risultato
        print(func.__name__, 'ha ritornato', str(res))
        return res
    return inner_func


@logged
def somma(*args):
    return sum(args)

# new_somma = logged(somma)


@logged
def crea_utente(nome, eta=0, nazione='Italia'):
    return (nome, eta, nazione)


somma(2, 78, 12, 45)
crea_utente('Domenico', 12, nazione='UK')

print(somma.__name__)
