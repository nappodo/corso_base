from giorno_4.cache import _get_cache_key


class User:
    def __init__(self):
        self.is_logged = False

    def call(self, arg):
        return str(arg)


def view_1(user, arg_a, arg_b):
    print(user.call(arg_a))
    print(user.call(arg_b))


def view_2(user, arg_a, arg_b):
    print(user.call(arg_a) + user.call(arg_b))


def view_1_no_deco(user, arg_a, arg_b):
    if not user.is_logged:
        print('User must be logged!')
        return
    print(user.call(arg_a))
    print(user.call(arg_b))


def view_2_no_deco(user, arg_a, arg_b):
    if not user.is_logged:
        print('User must be logged!')
        return False
    print(user.call(arg_a) + user.call(arg_b))


def view_login(user):
    user.is_logged = True
    return user


# ##### DECORATORE SEMPLICE

def user_logged(view_func):
    def inner_view(*args):
        user = args[0]
        if not user.is_logged:
            print('User must be logged!')
            return False
        res = view_func(*args)

        return res
    return inner_view


@user_logged
def view_1_dec(user, arg_a, arg_b):
    print(user.call(arg_a))
    print(user.call(arg_b))


@user_logged
def view_2_dec(user, arg_a, arg_b):
    print(user.call(arg_a) + user.call(arg_b))

# ##### DECORATORE con func_tool wrap
from functools import wraps


def user_logged_w_func_tools(view_func):
    @wraps(view_func)
    def inner_view(*args):
        user = args[0]
        if not user.is_logged:
            print('User must be logged!')
            return False
        return view_func(*args)
    return inner_view


def cached(view_func):
    @wraps(view_func)
    def inner_view(*args, **kwargs):
        from .cache import _internal_cache
        cache_key = _get_cache_key(view_func.__name__, args, kwargs)
        if cache_key in _internal_cache:
            return _internal_cache[cache_key]
        return view_func(*args, **kwargs)
    return inner_view


@cached
def myfunc(a, b, c):
    return a+b+c


@user_logged_w_func_tools
def view_1_dec_wrap(user, arg_a, arg_b):
    print(user.call(arg_a))
    print(user.call(arg_b))


@user_logged_w_func_tools
def view_2_dec_wrap(user, arg_a, arg_b):
    print(user.call(arg_a) + user.call(arg_b))


print(view_1.__name__)
print(view_1_dec.__name__)
print(view_1_dec_wrap.__name__)


def logged(func):
    @wraps(func)
    def inner_func(*args, **kwargs):
        # codice che logga i parametri
        res = func(*args, **kwargs)
        # codice che logga il risultato
        return res
    return inner_func


@logged
def mylogged_func(a, b, c):
    pass