"""
Scrivere un decoratore di funzione che faccia il controllo dei tipi degli argomenti.
Accetta come parametri i nomi delle variabili di una funzione e i tipi.
Esempio di utilizzo:

@checkargs(int, int, str, tuple)
def myfunc(a, b, c, d=(0,)):
    sum = a + b
    print('Sum: ', sum)
    print('String was: ', c)
    print('Tuple :', d)
"""

from functools import wraps
import inspect


def checkargs(*allowed_types):
    def check(func):
        @wraps(func)
        def new_func(*args, **kwargs):
            membs = inspect.getmembers(func)

            if not (len(args) + len(kwargs)) == len(allowed_types):
                raise Exception('Number of types in decorator must equal '
                                'the number of arguments defined in decorated function')

            for i, (a, t) in enumerate(zip(args, allowed_types)):
                if not isinstance(a, t):
                    raise ValueError('Bad type for arg {} at position {}. '
                                     'Must be of type {}'.format(a, i + 1, t))
            return func(*args)
        return new_func
    return check


@checkargs(int, int, str, (list, tuple))
def myfunc(a, b, c, d):
    print(a, b, c, d)

myfunc(1, 5, 'stringa', (1, 2, 3))
myfunc(1, 2, 'c', (0,))
myfunc(1, 2, 'b', (0, 0, 0))

#################################################
# # Type hinting in python 3 -
# serve solo per i tool di terze parti


def hinted(a: int, b: str) -> str:
    return str(a) + str(b)

print(hinted('a', 5))


def myfunc(a, b, l=()):
    c = a + b
    l = list(l)
    l.append(c)
    l = tuple(l)
    return l


def myfunc2(a, b, l=None):
    if l is None:
        l = []
    c = a + b
    l.append(c)
    return l

print('\n\n\n\n\n')
print(myfunc(2, 3))
print(myfunc(2, 8))
print(myfunc2(4, 5))
print(myfunc2(2, 5))
print(myfunc.__kwdefaults__)