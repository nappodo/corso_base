import json

data_1 = [1,
          2,
          'x',
          'text',
          'text_2',
          ]
data_2 = {'a': 5, 'b': 'xxxxxxx', 'l': data_1, }

dump_1= json.dumps(data_1)
dump_2= json.dumps(data_2)

print(dump_1)
print(dump_2)

print(type(dump_2))
data_11 = json.loads(dump_2)
print(type(data_11))
print(data_11)