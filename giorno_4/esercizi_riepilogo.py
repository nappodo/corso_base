"""
1. Using a list comprehension, create a new list called "newlist" out of the list
"numbers", which contains only the positive numbers from the list, as integers.

2. things = [3, 4, 6, 7, 0, 1]
# chaining together filter and map:
# first, filter to keep only the even numbers
# double each of them

3. write an equivalent version of 2. with list comprehension

4. Write a generator function with same functionalities of range(start, end, step)
"""


def myrange(start, end, step=1):
    yield start
    i = start + step
    while i < end:
        yield i
        i += step


for i in myrange(0, 100, 5):
    print(i)
