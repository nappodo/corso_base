#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Data un'url di un'immagine JPG/PNG, salvarla in un file su disco.
import os
from urllib import request, parse


def main(folder, img_url):
    if not os.path.exists(folder):
        os.makedirs(folder)
    img_name = parse.urlparse(img_url).path.split('/')[-1]
    img_path = os.path.join(folder, img_name)

    binary_content = read_image(img_url)
    save_file(img_path, binary_content)


def save_file(path, data, mode='wb'):
    with open(path, mode) as img_file:
        img_file.write(data)


def read_image(img_url):
    import requests
    res = requests.get(img_url)
    return res.content
    # res = request.urlopen(img_url)  # con urllib.request della libreria standard
    # return res.read()


if __name__ == '__main__':
    dest_folder = './'
    test_url = 'https://pixabay.com/static/uploads/photo/2016/09/03/15/13/seascapes-1641978_960_720.jpg'
    main(dest_folder, test_url)

#  Esercizio: riscrivere il programma usando argparse per aggiungere i parametri da linea di comando
