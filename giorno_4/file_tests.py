f1 = open('test_b.bin', mode='wb')
f2 = open('test_b.txt', mode='w')
content_bytes = b'012345566789aasaw\x0f\x34\577'
content_text = '012345566789aasaw\x04\x44\577'
f1.write(content_bytes)
f2.write(content_text)
f1.close()
f2.close()


text_to_write = """
Linea di testo 1
linea di test 2
Numero 0
Numero 1
"""

with open('test_c.txt', mode='w') as f:
    f.write(text_to_write)

with open('test_c.txt') as f:
    data = f.read()

print(type(data))
print(data)

with open('test_c.txt') as f:
    data = f.readlines()

print(type(data))
print(data)
