#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Esercizio classe 1. ##############
# Scrivere un programma che analizzi una stringa binaria
# in ingresso e stampi l'equivalente decimale

if __name__ == '__main__':
    s = input('Inserire stringa binaria: ')
    res = 0
    # for i, bit in enumerate(s[::-1]):
    for i in range(len(s), 0, -1):
        bit = s[i]
        if bit not in ('0', '1'):
            print('Stringa binaria non valida!!!')
            exit(1)
        if int(bit):
            res += 2 ** i
    print('Risultato {}'.format(res))
