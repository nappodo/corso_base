#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import math

if __name__ == '__main__':
    """
    commenti su più righe
    commento 1
    commento 2
    """
    r = float(input('Inserisci raggio R: '))
    area = r ** 2 * math.pi  # commento su riga
    print('Area cerchio: {:*^30.3f}'.format(area))
