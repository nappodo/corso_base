#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Esercizio classe 2. ##############
# Scrivere un programma che accetti in ingresso stringhe con caratteri
# 'a', 't', 'c, 'g' (DNA),
# utilizzando il metodi str.find, stampi l'indice della seconda occorrenza del carattere 'a'.
# Utilizzando, inoltre, il metodo str.rfind, stampare l'indice dell'ultima occorrenza della sottostringa 'atg'

if __name__ == '__main__':
    s = input('Inserire stringa DNA: ')
    for c in s:
        if c not in ('a', 't', 'c', 'g'):
            print('Errore. Stringa DNA non valida')
            exit(1)
    if 'a' not in s or 'atg' not in s:
        print('Sottostringhe non presenti')
        exit(1)
    first_index = s.find('a')
    second_index = s.find('a', first_index + 1)
    print('Indice della seconda occorrenza di "a": {}'.format(second_index))
    last_index = s.rfind('atg')
    print('Indice dell''ultima occorrenza di "atg": {}'.format(last_index))

