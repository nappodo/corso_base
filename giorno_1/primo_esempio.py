#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from datetime import date
import mysql.connector

config = {'user': 'synergia',
          'password': 'synergia',
          'host': 'localhost',
          'database': 'synergia',
          'port': 3306}

if __name__ == '__main__':
    # cnx = mysql.connector.connect(user='synergia', ..., port=3306)
    cnx = mysql.connector.connect(**config)
    print('Using database: {}'.format(cnx.database))

    cursor = cnx.cursor()
    cursor.execute('SHOW DATABASES')

    for (res, ) in cursor:
        print('DB: ', res)

    sql_create_table = ("""
    CREATE TABLE IF NOT EXISTS test_synergia(
      test_id INT NOT NULL AUTO_INCREMENT,
      test_title VARCHAR(100) NOT NULL,
      test_description VARCHAR(40) NOT NULL,
      submission_date DATE,
      PRIMARY KEY (test_id)
    ) ENGINE=InnoDB
    """)

    try:
        print('Creating table...')
        cursor.execute(sql_create_table)
        cnx.commit()
        print('Table created.')
    except mysql.connector.Error as err:
        print(err.msg)
    else:
        print('Inserting data...')
        insert_stmt = (
            """INSERT INTO test_synergia
               (test_title, test_description, submission_date)
               VALUES (%(title)s, %(desc)s, %(sub_date)s)"""
            )
        insert_data_1 = dict(title='Test Titolo 1', desc='Descrizione di test 1', sub_date=date(2015, 9, 14))
        insert_data_2 = dict(title='Test Titolo 2', desc='Descrizione di test 2', sub_date=date(2015, 11, 30))
        cursor.executemany(insert_stmt, [insert_data_1, insert_data_2])
        cnx.commit()
        print('{} rows inserted successfully'.format(cursor._rowcount))

        select_stmt = ("SELECT * FROM test_synergia WHERE submission_date > %s")
        select_data = date(2015, 10, 1)
        cursor.execute(select_stmt, (select_data,))

        for (_id, _title, _desc, _date) in cursor:
            print("{}, {}, {} submitted on {:%d %b %Y}".format(_id, _title, _desc, _date))

        drop_stmt = ("DROP TABLE test_synergia")
        print('Dropping table...')
        cursor.execute(drop_stmt)
        cnx.commit()
        print('All clear.')

    cursor.close()
    cnx.close()
