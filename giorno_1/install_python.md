Installazione e configurazione di un ambiente di sviluppo Python
================================================================

## 1. scaricare la versione 3.5.x per il proprio sistema operativo da https://www.python.org/downloads/
## 2. installare in windows e mac è molto semplice. Per Linux bisogna compilare oppure installare Python 3 con il gestore dei pacchetti della vostra distribuzione
+ Assicurarsi di avere alcune librerie necessarie a pip
    + sudo apt-get install zlib libssl-dev openssl (per ubuntu)
    + sudo yum install zlib libssl-devel openssl (per fedora)
+ tar xvf Python-3.5.2.tar.xz
+ cd Python-3.5.2
+ ./configure
+ make
+ sudo make install

## 3. Creare virtualenv
    C:\Python35\python C:\Python35\Tools\Scripts\pyvenv.py <path/to/venv>
    pyvenv <path/to/venv>

## 4. Attivare il virtualenv
    Windows: C:><path/to/venv>/Scripts/activate.bat
    Linux/Mac:   . <path/to/venv>/bin/activate
    
### Per disattivare:
    deactivate
    
    
    
    


    

    
    
    


