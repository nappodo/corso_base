#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# from collections import namedtuple
# from math import pi
#
# strings = ['{:.2f}'.format(pi),
#            '{:.4f}'.format(pi),
#            '{:+.2f}'.format(11),
#            '{:.0f}'.format(2.78421),
#            '{:0>2d}'.format(5),
#            '{:*<30s}'.format('text'),
#            '{:,}'.format(10000000),
#            '{:.2%}'.format(0.2213),
#            '{:.9e}'.format(1000002626484411),
#            '{:_>10d}'.format(12),
#            '{:_<10d}'.format(12),
#            '{:_^10d}'.format(12)]
#
# for s in strings:
#     input('Premi invio')
#     print(s)
#
# input()
# print('\n\n\n')
#
# print('='*30)
#
# Tuple = namedtuple('Tuple', ['a', 'b', 'c'])
#
# d1 = dict(a=1, b='text', c=45.5)
# print('{a} {b} {c}'.format(**d1))
# d2 = Tuple(a=100, b='lorum', c=49.5)
# print('{0.a} {0.b} {0.c}'.format(d2))
# input()
# print('\n\n\n')
# print('='*30)
# nome = 'Luigi'
# tipo = 'panino'
# farcitura = 'prosciutto'
# print('{} ha mangiato un {} al {}'.format(nome, tipo, farcitura))
# print('{nome} ha mangiato un {tipo_sandwich} al {farcitura}'.format(nome='Mario',
#                                                                     tipo_sandwich='toast',
#                                                                     farcitura='formaggio'))
s = "Units destroyed: {players[0]}"
l = ['Marco', 'Fabio']
# print(s.format(players=l))
# print(s.format(l))

fields = ['nome', 'cognome', 'eta']
s = ', '.join(fields)
cmd = 'SELECT {fields} FROM {table} '.format(fields=s, table='impiegati')
print(cmd)

splittt = '1# 2# 3# 4# 5'.split()
print(splittt)