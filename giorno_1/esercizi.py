# Nota aggiuntiva: attenzione ai valori mutabili!

a = 3
b = a

a = 5
print(b)
b = a
print(b)
a += 1
print(b)
input('Premi invio per continuare...')

a = [1, 2, 'x', 4]
b = a
a.append('a')
print(b)

# Esercizio in classe 1. ##############
# Scrivere un programma che analizzi una stringa binaria
# in ingresso e stampi l'equivalente decimale

# Esercizio 3. ##############
# Scrivere un programma che accetti in ingresso stringhe con caratteri
# 'a', 't', 'c, 'g' (DNA),
# utilizzando il metodi str.find, stampi
# l'indice della seconda occorrenza del carattere 'a'.
# Utilizzando, inoltre, il metodo str.rfind,
# stampare l'indice dell'ultima occorrenza della sottostringa 'atg'
