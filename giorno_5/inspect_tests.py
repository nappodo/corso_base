import inspect


# commento
def f(x, y):
    z = x + y
    return z


f_code = inspect.getsource(f)
inspect_code = inspect.getsource(inspect)
print(inspect_code)
print(inspect.getcomments(f))
