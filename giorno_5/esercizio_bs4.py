from collections import namedtuple

import bs4
import requests


News = namedtuple('News', ['title', 'url'])


def get_content(url):
    """
    Questa funzione utilizzerà il package requests per ottenere il contenuto della pagina
    :param url: url della pagina
    :return: HTML della pagina
    """
    pass


def get_repubblica_news(html):
    """
    Questa funzione userà bs4 per analizzare l'HTML ed estrarre i dati
    :param html: HTML
    :return: generatore di named tuples con titolo della notizia e url
    """
    pass


def save_news(news):
    """
    Salva le news su file
    :param news: Oggetto iterabile di tuple News
    :return: None
    """
    pass


def run(url):
    # main function
    html_content = get_content(url)
    news = get_repubblica_news(html_content)
    save_news(news)


if __name__ == '__main':
    url = 'http://www.repubblica.it'
    run(url)
