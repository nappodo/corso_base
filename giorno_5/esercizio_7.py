import importlib
from collections import namedtuple

Customer = namedtuple('Customer', ['type'])


def render(customer):
    type_ = customer.type
    try:
        mod = importlib.import_module('giorno_5.module_{}'.format(type_.lower()))
    except ImportError:
        mod = importlib.import_module('giorno_5.module_base')
    print('Importato il modulo ', mod.__file__)
    RendererClass = mod.Renderer
    renderer = RendererClass()
    renderer.render()
