import math

discount_functions = {
    'premium': lambda x: x - x * 15 / 100,
    'gold': lambda x: x - x * 10 / 100,
    'silver': lambda x: x - x * 7.5 / 100,
    'base': lambda x: x - x * 5 / 100,
}


def premium_discount(price):
    return price - price * 15 / 100


discount_functions_no_lambda = {
    'premium': premium_discount,
    'gold': lambda x: x - x * 10 / 100,
    'silver': lambda x: x - x * 7.5 / 100,
    'base': lambda x: x - x * 5 / 100,
}


def calculate_discount(client_type, price):
    return discount_functions.get(client_type, lambda x: x)(price)


print(calculate_discount('premium', 150000))
print(calculate_discount('base', 1000))
print(calculate_discount('gold', 150000))
print(calculate_discount('n/a', 150000))


"""
#########################################
Modo classico: if elif elif ... elif else
"""


def calculate_discount_2(client_type, price):
    """
    In Java sarebbe
    function calculateDiscount(client_type, price) {
      switch(client_type) {
          case 'premium':
              return price*5/100
          case 'gold':
         .....
      }
    }
    """
    if client_type == 'premium':
        return price - price * 15 / 100
    elif client_type == 'gold':
        return price - price * 10 / 100
    elif client_type == 'silver':
        return price - price * 7.5 / 100
    elif client_type == 'base':
        return price - price * 5 / 100
    else:
        return price

print('\n\nClassic mode **************')
print(calculate_discount_2('premium', 150000))
print(calculate_discount_2('base', 1000))
print(calculate_discount_2('gold', 150000))
print(calculate_discount_2('n/a', 150000))


# l = [9, 4, 8, 16, 23]
# l.sort(key=math.sqrt, reverse=True)
# print(l)