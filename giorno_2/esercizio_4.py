import random


def randoms():
    random_numbers = random.sample(range(1000), 50)
    l = [i for i in random_numbers if i % 3 == 0]
    print(str(l))


if __name__ == '__main__':
    randoms()
