from collections import namedtuple

# Una namedtuple serve per implementare un "ValueObject"
Calciatore = namedtuple('Calciatore', 'eta, squadra, stipendio')

totti = Calciatore(eta=40, squadra='Roma', stipendio=130222330)
rossi = Calciatore(eta=21, squadra='Juventus', stipendio=32223)
de_rossi = Calciatore(eta=21, squadra='Juventus', stipendio=332432453)

d = {'totti': totti,
     'rossi': rossi,
     'de rossi': de_rossi}

salaries = []  # si potrebbe definire direttamente su una riga con una list comprehension
for name, value in d.items():
    salaries.append(value.stipendio)
    print(name.title(), 'gioca nella', value.squadra, sep=' ')

totale = sum(salaries)
print('\nTotale stipendi:', str(totale), sep=' ')
