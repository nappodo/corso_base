import random

random_nums = random.sample(range(1, 1000), 100)
l = []  # list()
for num in random_nums:  # si può fare su una riga con la list comprehension
    if num % 3 == 0:
        l.append(num)
res = set(l)  # elimina duplicati
print(res)
