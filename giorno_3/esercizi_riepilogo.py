"""
Esercizio 1
-----------
Creare due tuple che rappresentino i due
elenchi di nomi e cognomi descritti sotto:
nomi: Numa, Tullo, Anco
cognomi: Pompilio, Ostilio, Marzio

Ottenere una lista in cui ogni elemento è un
dizionario {'nome': nome, 'cognome':
cognome}, che accoppia nomi e cognomi in
base all'ordine

Esercizio 2
-----------

Creare un dizionario che contenga come chiavi 'nome' e 'cognome',
inserendo i propri dati come valori

Aggiungere 'matricola'
Aggiungere 'esami', provando ad immaginare che tipi di dato usare
per rappresentare sia nome che voto degli esami

Esercizio 3
-----------
Calcolare la somma dei primi 500 numeri naturali (da 0 a 500 escluso)
e stamparla a video. L'esercizio deve essere svolto con una sola riga di codice.


Esercizio 4
-----------
Data la stringa 'abcdefghi', scrivere un programma che analizzi la stringa e
stampi a video:
Lettera 1: a
Lettera 2: b
...
E così via.

Modificare poi il programma in modo da leggere la stringa da tastiera.
"""