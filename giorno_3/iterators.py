from itertools import *

for i in chain([1, 2, 3], ['a', 'b', 'c']):
    print(i)

input()
print('Stop at 5:')
for i in islice(count(), 5):
    print(i)
input()
print('Start at 5, Stop at 10:')
for i in islice(count(), 5, 10):
    print(i)
input()
print('By tens to 100:')
for i in islice(count(), 0, 100, 10):
    print(i)
input()

r = islice(count(), 5)
i1, i2 = tee(r)

for i in i1:
    print('i1:', i)
for i in i2:
    print('i2:', i)

input()
r = islice(count(), 5)
i1, i2 = tee(r)

for i in r:
    print('r:', i)
    if i > 1:
        break
for i in i1:
    print('i1:', i)
for i in i2:
    print('i2:', i)
input()

i = 0
for item in cycle(['a', 'b', 'c']):
    i += 1
    if i == 10:
        break
    print(i, item)

input()


def should_drop(x):
    print('Testing:', x)
    return x < 1

for i in dropwhile(should_drop, [-1, 0, 1, 2, 3, 4, 1, -2 ]):
    print('Yielding:', i)
input()


def should_take(x):
    print('Testing:', x)
    return x < 2

for i in takewhile(should_take, [ -1, 0, 1, 2, 3, 4, 1, -2 ]):
    print('Yielding:', i)

input()

from operator import itemgetter


def order_dict(item):
    key = item[0]
    val = item[1]
    return str(val)+key

d = dict(a=1, b=2, c=1, d=2, e=1, f=2, g=3)
print(list(d.items()))
di = sorted(d.items(), key=itemgetter(1))
print(di)
for k, g in groupby(di, key=itemgetter(1)):
    print(k, list(map(itemgetter(1), g)))
