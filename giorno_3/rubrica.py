import os
import pickle

FILENAME = 'rubrica.pickle'

MENU = '''
---------------------------
l: lista contatti
i: inserisci nuovo contatto
m: modifica contatto
c: cancella contatto
s: salva rubrica
x: Esci
---------------------------
'''


def list_phonebook(phonebook):
    if not phonebook:
        print('>>>>>>>>>>> Rubrica Vuota <<<<<<<<<<<<<')
        return
    for name, phone in phonebook.items():
        print('{}: {}'.format(name, phone))


def add(phonebook):
    s = input('Inserisci contatto (es. Nome:06555555) ###  ')
    name, phone = s.split(':')
    phonebook[name] = phone
    print('Contatto {} inserito/aggiornato'.format(name))


def update(phonebook):
    add(phonebook)


def remove(phonebook):
    s = input('Inserisci nome da eliminare ###  ')
    if s in phonebook:
        del phonebook[s]
        print('Contatto {} eliminato'.format(s))
    else:
        print('Contatto {} non presente in rubrica'.format(s))


def save(phonebook):
    pickle.dump(phonebook, open(FILENAME, 'wb'))
    print('Rubrica salvata su disco')


def quit_phonebook(phonebook):
    save(phonebook)
    print('Buona giornata!!!')
    exit()


def menu():
    print(MENU)


def main():
    commands = {'l': list_phonebook,
                'i': add,
                'm': update,
                'c': remove,
                's': save,
                'x': quit_phonebook}
    phonebook = pickle.load(open(FILENAME, 'rb')) if os.path.exists(FILENAME) else {}
    while True:
        menu()
        command = input('Rubrica Telefonica >  ')
        if command in commands:
            func = commands[command]
            func(phonebook)
        else:
            print('Comando non accettato! riprova...')


if __name__ == '__main__':
    main()
