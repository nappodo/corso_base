print(sorted([5, 2, 3, 1, 4]))

# key
print(sorted("This is a test string from Andrew".split()))
print(sorted("This is a test string from Andrew".split(), key=str.lower))

student_tuples = [
        ('john', 'A', 15),
        ('jane', 'B', 12),
        ('dave', 'B', 10),
]
print(sorted(student_tuples, key=lambda student: student[2]))   # sort by age


from operator import itemgetter
print(sorted(student_tuples, key=itemgetter(2)))
print(sorted(student_tuples, key=itemgetter(1, 2)))

# reverse
print(sorted(student_tuples, key=itemgetter(2), reverse=True))

# dizionari
d = {1: 'D', 2: 'B', 3: 'B', 4: 'E', 5: 'A'}
from collections import OrderedDict
print(sorted({1: 'D', 2: 'B', 3: 'B', 4: 'E', 5: 'A'}))
print(OrderedDict(sorted(d.items(), key=itemgetter(1))))
