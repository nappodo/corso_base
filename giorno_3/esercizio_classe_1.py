def _is_float(s):
    elems = s.partition('.')
    return elems[0].isdigit() and elems[1] == '.' and elems[2].isdigit()


def num():
    while True:
        # aspetta input utente. r conterrà una stringa
        r = input(' Inserisci un numero >  ')
        if r == 'quit':
            return
        if not (r.isdigit() or _is_float(r)):
            print('Inserire un numero valido')
        else:
            # a questo punto siamo sicuri che r è una stringa numerica
            yield float(r)


def main():
    print(sum(num()))

if __name__ == '__main__':
    main()



